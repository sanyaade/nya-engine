//nya-engine (C) nyan.developer@gmail.com released under the MIT license (see LICENSE)

package nya;

import android.app.Activity;
import android.os.Bundle;
import android.content.res.AssetManager;
import android.view.*;
import android.widget.RelativeLayout;

public class native_activity extends Activity implements SurfaceHolder.Callback, View.OnTouchListener
{
    protected static ViewGroup m_layout;
    protected static SurfaceView m_view;
    protected static AssetManager m_asset_mgr;
    private static boolean m_library_loaded;

    protected void onLoadNativeLibrary() //override to load your own
    {
        System.loadLibrary("nya_native");
    }

    protected void onSpawnMain() //override to spawn manually
    {
        native_spawn_main();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        if(!m_library_loaded)
        {
            onLoadNativeLibrary();
            m_library_loaded=true;
        }

        super.onCreate(savedInstanceState);

        m_asset_mgr=getAssets();
        native_set_asset_mgr(m_asset_mgr);

        m_view = new SurfaceView(getApplication());
        m_view.getHolder().addCallback(this);
        m_view.setOnTouchListener(this);

        m_layout = new RelativeLayout(this);
        m_layout.addView(m_view);
        setContentView(m_layout);

        onSpawnMain();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        native_resume();
    }
    
    @Override
    protected void onPause()
    {
        super.onPause();
        native_pause();
    }

    @Override
    protected void onDestroy()
    {
        native_exit();
        super.onDestroy();
        m_layout = null;
        m_view = null;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        switch(event.getActionMasked())
        {
            case MotionEvent.ACTION_MOVE:
                for(int i = 0;i<event.getPointerCount();++i)
                    native_touch((int)event.getX(i),(int)event.getY(i),event.getPointerId(i),true);
                break;

            case MotionEvent.ACTION_UP:
                native_touch((int)event.getX(0),(int)event.getY(0),event.getPointerId(0),false);
                break;
            case MotionEvent.ACTION_DOWN:
                native_touch((int)event.getX(0),(int)event.getY(0),event.getPointerId(0),true);
                break;

            case MotionEvent.ACTION_POINTER_UP:
                int i=event.getActionIndex();
                native_touch((int)event.getX(i),(int)event.getY(i),event.getPointerId(i),false);
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                i=event.getActionIndex();
                native_touch((int)event.getX(i),(int)event.getY(i),event.getPointerId(i),true);
                break;

            default:
                break;
        }

        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        return native_key(keyCode,true);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event)
    {
        return native_key(keyCode,false);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h)
    {
        native_set_surface(holder.getSurface());
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {}

    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        native_set_surface(null);
    }

    public static native void native_spawn_main();
    public static native void native_resume();
    public static native void native_pause();
    public static native void native_exit();
    public static native void native_touch(int x,int y,int id,boolean pressed);
    public static native boolean native_key(int code,boolean pressed);
    public static native void native_set_surface(Surface surface);
    public static native void native_set_asset_mgr(AssetManager mgr);
}
