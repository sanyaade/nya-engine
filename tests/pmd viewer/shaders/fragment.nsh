@sampler base "diffuse"
@sampler toon "toon"
@sampler env_add "env add"
@sampler env_mult "env mult"

@uniform light_dir "light dir"=0.4,0.82,0.4
@uniform light_color "light color"=1.0,1.0,1.0
@predefined cam_pos "nya camera position":local

@uniform amb_k "amb k"
@uniform diff_k "diff k"
@uniform spec_k "spec k"

@all

varying vec2 tc;
varying vec2 env_tc;
varying vec3 normal;
varying vec3 pos;

@fragment

uniform sampler2D base;
uniform sampler2D toon;
uniform sampler2D env_add;
uniform sampler2D env_mult;

uniform vec4 light_dir;
uniform vec4 light_color;
uniform vec4 cam_pos;

uniform vec4 amb_k;
uniform vec4 diff_k;
uniform vec4 spec_k;

void main()
{
    vec4 c = texture2D(base,tc);
    c.a *= diff_k.a;
    if(c.a < 0.01)
        discard;

    float s = 0.0;
    float l = 0.5 + 0.5 * dot(light_dir.xyz, normal);
    vec3 t = texture2D(toon,vec2(s, l)).rgb;

    c.rgb *= texture2D(env_mult, env_tc).rgb;
    c.rgb += texture2D(env_add, env_tc).rgb;

    vec3 eye = normalize(cam_pos.xyz - pos);
    float ndh = dot(normal, normalize(light_dir.xyz + eye));
    vec3 spec = light_color.rgb * spec_k.rgb * max(pow(ndh, spec_k.a), 0.0);

    c.rgb *= clamp(amb_k.rgb + diff_k.rgb * light_color.rgb, vec3(0.0), vec3(1.0));
    c.rgb += spec;

    c.rgb *= t;

    gl_FragColor = c;
}
