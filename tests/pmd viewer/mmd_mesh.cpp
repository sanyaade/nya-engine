//nya-engine (C) nyan.developer@gmail.com released under the MIT license (see LICENSE)

#include "mmd_mesh.h"

bool mmd_mesh::load(const char *name)
{
    unload();

    nya_scene::mesh::register_load_function(pmx_loader::load,false);
    nya_scene::mesh::register_load_function(pmd_loader::load,false);

    if(!mesh::load(name))
        return false;

    if(!init_vbo())
        return false;

    update(0);
    return true;
}

bool mmd_mesh::init_vbo()
{
    m_pmd_data=pmd_loader::get_additional_data(*this);
    m_pmx_data=pmx_loader::get_additional_data(*this);

    if(!is_mmd())
        return true;

    const nya_render::vbo &vbo=internal().get_shared_data()->vbo;
    nya_memory::tmp_buffer_ref buf;
    if(!vbo.get_vertex_data(buf))
        return false;

    m_vertex_data.resize(vbo.get_vert_stride()*vbo.get_verts_count()/4);
    buf.copy_to(&m_vertex_data[0],m_vertex_data.size()*4);
    buf.free();

    if(get_skeleton().get_bones_count()>pmd_loader::gpu_skining_bones_limit)
        m_original_vertex_data=m_vertex_data;

    if(m_pmd_data)
        m_morphs.resize(m_pmd_data->morphs.size());
    else if(m_pmx_data)
        m_morphs.resize(m_pmx_data->morphs.size());

    m_vbo.set_vertex_data(&m_vertex_data[0],vbo.get_vert_stride(),vbo.get_verts_count());
    m_vbo.set_vertices(vbo.get_vert_offset(),vbo.get_vert_dimension());
    m_vbo.set_normals(vbo.get_normals_offset());
    for(int i=0;i<nya_render::vbo::max_tex_coord;++i)
    {
        if(vbo.get_tc_dimension(i)>0)
            m_vbo.set_tc(i,vbo.get_tc_offset(i),vbo.get_tc_dimension(i));
    }

    return true;
}

void mmd_mesh::unload()
{
    mesh::unload();
    m_vertex_data.clear();
    m_original_vertex_data.clear();
    m_pmd_data=0;
    m_pmx_data=0;
    m_vbo.release();
    m_morphs.clear();
}

void mmd_mesh::set_anim(const nya_scene::animation &anim,int layer)
{
    mesh::set_anim(anim,layer);
    int idx=-1;
    for(int i=0;i<int(m_anims.size());++i)
    {
        if(m_anims[i].layer==layer)
        {
            idx=i;
            break;
        }
    }

    if(idx<0)
    {
        idx=int(m_anims.size());
        m_anims.resize(idx+1);
    }

    m_anims[idx].curves_map.clear();
    m_anims[idx].curves_map.resize(m_morphs.size());
    if(!anim.get_shared_data().is_valid())
        return;

    if(m_pmd_data)
    {
        for(int i=0;i<int(m_morphs.size());++i)
            m_anims[idx].curves_map[i]=anim.get_shared_data()->anim.get_curve_idx(m_pmd_data->morphs[i].name.c_str());
    }
    else if(m_pmx_data)
    {
        for(int i=0;i<int(m_morphs.size());++i)
            m_anims[idx].curves_map[i]=anim.get_shared_data()->anim.get_curve_idx(m_pmx_data->morphs[i].name.c_str());
    }
}

void mmd_mesh::update(unsigned int dt)
{
    mesh::update(dt);

    if(!is_mmd())
        return;

    for(int i=0;i<m_anims.size();++i)
    {
        const nya_scene::animation_proxy &ap=get_anim(m_anims[i].layer);
        if(ap.is_valid())
        {
            if(ap->get_shared_data().is_valid())
            {
                const nya_render::animation &a=ap->get_shared_data()->anim;
                if(i==0)
                {
                    for(int j=0;j<m_anims[i].curves_map.size();++j)
                    {
                        if(m_morphs[j].overriden)
                            continue;

                        m_morphs[j].value=a.get_curve(m_anims[i].curves_map[j],get_anim_time(m_anims[i].layer))*ap->get_weight();
                    }
                }
                else
                {
                    for(int j=0;j<m_anims[i].curves_map.size();++j)
                    {
                        if(m_morphs[j].overriden)
                            continue;

                        m_morphs[j].value+=a.get_curve(m_anims[i].curves_map[j],get_anim_time(m_anims[i].layer))*ap->get_weight();
                    }
                }
            }
        }
    }

    //update group morphs first

    if(m_pmx_data)
    {
        for(int i=0;i<(int)m_pmx_data->morphs.size();++i)
        {
            const pmx_loader::additional_data::morph &m=m_pmx_data->morphs[i];
            if(m.type!=pmx_loader::morph_type_group)
                continue;

            const float delta=m_morphs[i].value-m_morphs[i].last_value;
            if(fabsf(delta)<0.02f)
                continue;

            const pmx_loader::additional_data::group_morph &gm=m_pmx_data->group_morphs[m.idx];
            for(int j=0;j<(int)gm.morphs.size();++j)
                m_morphs[gm.morphs[j].first].value=m_morphs[i].value*gm.morphs[j].second;

            m_morphs[i].last_value=m_morphs[i].value;
        }
    }

    //not optimal, just for testing

    int stride=m_vbo.get_vert_stride()/4;
    if(!stride)
        return;

    bool need_update_vbo=false;
    if(!m_original_vertex_data.empty())
    {
        need_update_vbo=true;

        const nya_render::skeleton &sk=get_skeleton();
        if(m_pmx_data)
        {
            const pmx_loader::vert *verts=(const pmx_loader::vert *)&m_original_vertex_data[0];
            for(int i=0;i<m_vbo.get_verts_count();++i)
            {
                nya_math::vec3 pos;
                for(int j=0;j<4;++j)
                {
                    if(verts[i].bone_weight[j]>0.001f)
                    {
                        const int idx=verts[i].bone_idx[j];
                        pos+=(sk.get_bone_pos(idx)+sk.get_bone_rot(idx).rotate(verts[i].pos-sk.get_bone_original_pos(idx)))*verts[i].bone_weight[j];
                    }
                }

                memcpy(&m_vertex_data[i*stride],&pos,sizeof(pos));
            }
        }
        else
        {
            const pmd_loader::vert *verts=(const pmd_loader::vert *)&m_original_vertex_data[0];
            for(int i=0;i<m_vbo.get_verts_count();++i)
            {
                nya_math::vec3 pos;
                for(int j=0;j<2;++j)
                {
                    const int idx=verts[i].bone_idx[j];
                    pos+=(sk.get_bone_pos(idx)+sk.get_bone_rot(idx).rotate(verts[i].pos-sk.get_bone_original_pos(idx)))*
                         (j==0?verts[i].bone_weight:(1.0f-verts[i].bone_weight));
                }

                memcpy(&m_vertex_data[i*stride],&pos,sizeof(pos));
            }
        }
    }

    for(int i=0;i<m_morphs.size();++i)
    {
        const float delta=m_morphs[i].value-m_morphs[i].last_value;
        if(fabsf(delta)<0.02f)
            continue;

        if(m_pmd_data)
        {
            const pmd_morph_data::morph &m=m_pmd_data->morphs[i];
            for(int j=0;j<int(m.verts.size());++j)
            {
                const unsigned int base=m.verts[j].idx*stride;
                m_vertex_data[base]+=m.verts[j].pos.x*delta;
                m_vertex_data[base+1]+=m.verts[j].pos.y*delta;
                m_vertex_data[base+2]+=m.verts[j].pos.z*delta;
            }
        }
        else if(m_pmx_data)
        {
            const pmx_loader::additional_data::morph &m=m_pmx_data->morphs[i];
            switch(m.type)
            {
                case pmx_loader::morph_type_vertex:
                {
                    const pmx_loader::additional_data::vert_morph &vm=m_pmx_data->vert_morphs[m.idx];
                    for(int j=0;j<int(vm.verts.size());++j)
                    {
                        const unsigned int base=vm.verts[j].idx*stride;
                        m_vertex_data[base]+=vm.verts[j].pos.x*delta;
                        m_vertex_data[base+1]+=vm.verts[j].pos.y*delta;
                        m_vertex_data[base+2]+=vm.verts[j].pos.z*delta;
                    }
                }
                break;

                case pmx_loader::morph_type_material:
                {
                    const float value=m_morphs[i].value;
                    const pmx_loader::additional_data::mat_morph &mm=m_pmx_data->mat_morphs[m.idx];
                    for(int j=0;j<int(mm.mats.size());++j)
                    {
                        const pmx_loader::additional_data::morph_mat &m=mm.mats[j];
                        const pmx_loader::additional_data::mat &om=m_pmx_data->materials[m.mat_idx];
                        nya_scene::material &mat=modify_material(m.mat_idx);

                        pmx_loader::pmx_material_params params;
                        pmx_loader::pmx_edge_params edge;

                        if(m.op==pmx_loader::additional_data::morph_mat::op_add)
                        {
                            params.ambient=om.params.ambient+m.params.ambient*value;
                            params.diffuse=om.params.diffuse+m.params.diffuse*value;
                            params.specular=om.params.specular+m.params.specular*value;
                            params.shininess=om.params.shininess+m.params.shininess*value;

                            edge.color=om.edge_params.color+m.edge_params.color*value;
                            edge.width=om.edge_params.width+m.edge_params.width*value;
                        }
                        else if(m.op==pmx_loader::additional_data::morph_mat::op_mult)
                        {
                            params.ambient=nya_math::vec3::lerp(om.params.ambient,om.params.ambient*m.params.ambient,value);
                            params.diffuse=nya_math::vec4::lerp(om.params.diffuse,om.params.diffuse*m.params.diffuse,value);
                            params.specular=nya_math::vec3::lerp(om.params.specular,om.params.specular*m.params.specular,value);
                            params.shininess=nya_math::lerp(om.params.shininess,om.params.shininess*m.params.shininess,value);

                            edge.color=nya_math::vec4::lerp(om.edge_params.color,om.edge_params.color*m.edge_params.color,value);
                            edge.width=nya_math::lerp(om.edge_params.width,om.edge_params.width*m.edge_params.width,value);
                        }

                        mat.set_param(mat.get_param_idx("amb k"),params.ambient,1.0f);
                        mat.set_param(mat.get_param_idx("diff k"),params.diffuse);
                        mat.set_param(mat.get_param_idx("spec k"),params.specular,params.shininess);

                        if(om.edge_group_idx>=0)
                        {
                            nya_scene::material &mat=modify_material(om.edge_group_idx);
                            mat.set_param(mat.get_param_idx("edge offset"),edge.width*0.02f,edge.width*0.02f,edge.width*0.02f,0.0f);
                            mat.set_param(mat.get_param_idx("edge color"),edge.color);
                        }
                    }
                }
                break;

                default: continue;
            }
        }

        need_update_vbo=true;
        m_morphs[i].last_value=m_original_vertex_data.empty()?m_morphs[i].value:0.0f;
    }

    if(need_update_vbo)
        m_vbo.set_vertex_data(&m_vertex_data[0],m_vbo.get_vert_stride(),m_vbo.get_verts_count());
}

void mmd_mesh::draw_group(int group_idx,const char *pass_name) const
{
    if(!is_mmd())
    {
        mesh::draw_group(group_idx,pass_name);
        return;
    }

    if(!internal().get_shared_data().is_valid())
        return;

    if(group_idx<0 || group_idx>=get_groups_count())
        return;

    /*
     if(m_mesh.internal().m_has_aabb) //ToDo
     {
     if(get_camera().is_valid() && !get_camera()->get_frustum().test_intersect(get_aabb()))
     return;
     }
     */
    nya_scene::transform::set(internal().get_transform());
    nya_scene::shader_internal::set_skeleton(&get_skeleton());

    const nya_scene::shared_mesh &sh=*internal().get_shared_data().operator ->();
    sh.vbo.bind_indices();
    m_vbo.bind_verts();

    const nya_scene::material &m=get_material(group_idx);
    m.internal().set(pass_name);
    m_vbo.draw(sh.groups[group_idx].offset,sh.groups[group_idx].count);
    m.internal().unset();
    m_vbo.unbind();
    sh.vbo.unbind();
    nya_scene::shader_internal::set_skeleton(0);
}

void mmd_mesh::draw(const char *pass_name) const
{
    if(!is_mmd())
    {
        mesh::draw(pass_name);
        return;
    }

    for(int i=0;i<get_groups_count();++i)
        draw_group(i);
}

const char *mmd_mesh::get_morph_name(int idx) const
{
    if(idx<0 || idx>=get_morphs_count())
        return 0;

    if (m_pmd_data)
        return m_pmd_data->morphs[idx].name.c_str();

    if (m_pmx_data)
        return m_pmx_data->morphs[idx].name.c_str();

    return 0;
}

pmd_morph_data::morph_kind mmd_mesh::get_morph_kind(int idx) const
{
    if(idx<0 || idx>=get_morphs_count())
        return pmd_morph_data::morph_base;

    if (m_pmd_data)
        return m_pmd_data->morphs[idx].kind;

    if (m_pmx_data)
        return m_pmx_data->morphs[idx].kind;

    return pmd_morph_data::morph_base;
}

float mmd_mesh::get_morph(int idx) const
{
    if(idx<0 || idx>=get_morphs_count())
        return 0.0f;

    return m_morphs[idx].value;
}
