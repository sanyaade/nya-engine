//nya-engine (C) nyan.developer@gmail.com released under the MIT license (see LICENSE)

#pragma once

#include "scene/mesh.h"

#include "load_pmd.h"
#include "load_pmx.h"

class mmd_mesh: public nya_scene::mesh
{
public:
    bool load(const char *name);
    void unload();

    void set_anim(const nya_scene::animation &anim,int layer=0);

    void set_morph(int idx,float value,bool overriden)
    {
        if(idx<0 || idx>=int(m_morphs.size()))
            return;

        m_morphs[idx].value=value;
        m_morphs[idx].overriden=overriden;
    }

    void update(unsigned int dt);
    void draw(const char *pass_name=nya_scene::material::default_pass) const;
    void draw_group(int group_idx,const char *pass_name=nya_scene::material::default_pass) const;

    int get_morphs_count() const { return (int)m_morphs.size(); }
    const char *get_morph_name(int idx) const;
    pmd_morph_data::morph_kind get_morph_kind(int idx) const;
    float get_morph(int idx) const;

    bool is_mmd() const { return m_pmd_data!=0 || m_pmx_data!=0; }

    mmd_mesh(): m_pmd_data(0),m_pmx_data(0) {}

    mmd_mesh(const mmd_mesh &from): mesh(from) { m_vbo=nya_render::vbo(); init_vbo(); }

    mmd_mesh &operator = (const mmd_mesh &from)
    {
        if(this==&from)
            return *this;

        this->~mmd_mesh();
        return *new(this)mmd_mesh(from);
    }

private:
    bool init_vbo();

    nya_render::vbo m_vbo;
    std::vector<float> m_vertex_data;
    std::vector<float> m_original_vertex_data;
    const pmd_loader::additional_data *m_pmd_data;
    const pmx_loader::additional_data *m_pmx_data;

    struct morph
    {
        bool overriden;
        float value;
        float last_value;

        morph(): overriden(false),value(0.0f),last_value(0.0f) {}
    };

    std::vector<morph> m_morphs;

    struct applied_anim { int layer; std::vector<int> curves_map; };
    std::vector<applied_anim> m_anims;
};
