//nya-engine (C) nyan.developer@gmail.com released under the MIT license (see LICENSE)

#pragma once

#include <vector>
#include <string>

namespace nya_formats
{

class math_expr_parser
{
public:
    typedef void (*function)(float *args,float *return_value);
    void set_function(const char *name,int args_count,int return_count,function f);
    void set_constant(const char *name,float value);

public:
    bool parse(const char *expr);

public:
    int get_var_idx(const char *name) const;
    int get_vars_count() const;
    const char *get_var_name(int idx) const;
    bool set_var(const char *name,float value,bool allow_unfound=true);
    bool set_var(int idx,float value);
    const float *get_vars() const;
    float *get_vars();

public:
    float calculate() const;

public:
    math_expr_parser(): m_ops_count(0) {}
    math_expr_parser(const char *expr) { parse(expr); }

private:
    int add_var(const char *name);
    template<typename t> float calculate(t &stack) const;

private:
    std::vector<std::pair<std::string,float> > m_constants;
    std::vector<float> m_vars;
    std::vector<std::string> m_var_names;
    std::vector<int> m_ops;

    struct user_function { std::string name; char args_count,return_count; function f; };
    std::vector<user_function> m_functions;

    class stack_validator
    {
    public:
        void clear() { m_current_size=0; }
        void add(float f) { if(++m_current_size>m_size) m_size=m_current_size; }
        float &get() { static float v=0.0f; if(m_current_size<=0) m_valid=false; return v; }
        void pop() { if(--m_current_size<0) m_valid=false; }
        void call(const user_function &f);

    public:
        bool is_valid() { return m_valid; }
        int get_size() { return m_size; }

    public:
        stack_validator(): m_valid(true),m_size(0),m_current_size(0) {}

    private:
        bool m_valid;
        int m_size,m_current_size;
    };

    class stack
    {
    public:
        void clear() { m_pos=0; }
        void add(const float &f) { m_buf[++m_pos]=f; }
        float &get() { return m_buf[m_pos]; }
        void pop() { --m_pos; }
        void call(const user_function &f);

    public:
        //m_buf[0] reserved for constant state
        void set_size(int size) { m_buf.resize(size+1,0.0f); m_pos=0; }
        void set_constant(float value) { m_buf.resize(1); m_buf[0]=value; m_pos=0; }

    public:
        stack() { set_constant(0.0f); }

    private:
        std::vector<float> m_buf;
        int m_pos;
    };

    mutable stack m_stack;
    int m_ops_count;
};

}
